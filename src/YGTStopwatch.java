/**
 * Created by Ebubekir.
 */
public class YGTStopwatch {

    /*
          Usage:  Create an YGTStopwatch object and do your work. And call elapsedTime() function.

          YGTStopwatch ygt = new YGTStopwatch();
          // do something
          double time = ygt.elapsedTime();
     */

    private final long start;

    public YGTStopwatch() {
        start = System.nanoTime();
    }

    public double elapsedTime() {
        long now = System.nanoTime();
        return (now - start) / 1000000000.0;
    }

    public long getCurrentTimeInMillis(){
        return System.currentTimeMillis();
    }

    public long getCurrentTimeInNano(){
        return System.nanoTime();
    }


}


