import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Ebubekir.
 */
public class BinarySearch<Vector> {

    private ArrayList<Vector> arrayList;            // A sorted arrayList
    private Comparator<Vector> vectorComparator;    // Search based Comparator
    Vector key;                                     // Search key
    private int index;                              // returning index


    /**
     * This constructor returns an index by performing a binary search on a given sequential array.
     * @param arrayList sorted list
     * @param vectorComparator search based comparator
     * @param key search key
     */
    public BinarySearch(ArrayList<Vector> arrayList, Comparator<Vector> vectorComparator, Vector key){
        if (arrayList == null || vectorComparator == null || key == null){
            throw new IllegalArgumentException("Parameter Objects cannot be null");
        }
        this.arrayList = arrayList;
        this.vectorComparator = vectorComparator;
        this.key = key;
        this.index = binarySearch();
    }

    public int getIndex() {
        return index;
    }

    /**
     * It does a binary search and
     * finds an element that matches the criterion provided by the "comparator".
     * And it returns the index in the array.
     * @return founded index (Returns the nagative index if no element is found.)
     */
    private int binarySearch(){
        int lower = 0;
        int higher = arrayList.size()-1;

        while (lower <= higher){
            int middle = lower + (higher - lower)/2;
            Vector middleElement = arrayList.get(middle);
            int compare = vectorComparator.compare(middleElement,key);
            if (compare > 0){
                // on array's left
                higher = middle - 1;
            }
            else if (compare < 0){
                // on array's right
                lower = middle + 1;
            }
            else if (compare == 0) {
                return middle;  // key found middle
            }
        }
        return -(lower + 1);  // key not found
    }


    /**
     *
     * @return an array holding the elements that match the criterion.
     */
    public ArrayList<Vector> getFoundedKeys(){
        ArrayList<Vector> tempList = new ArrayList<Vector>();

        // control the Key's left in array
        for (int i = getIndex(); i != 0; i--){
            Vector temp = arrayList.get(i);
            int compare = vectorComparator.compare(temp,key);
            if (compare != 0){
                break;
            }
            else {
                tempList.add(arrayList.get(i));
            }
        }

        // control the Key's right in array
        for (int i = getIndex()+1; i < arrayList.size(); i++){
            Vector temp = arrayList.get(i);
            int compare = vectorComparator.compare(temp,key);
            if (compare != 0){
                break;
            }
            else {
                tempList.add(arrayList.get(i));
            }
        }
        return tempList;
    }
}
