import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws IOException {

        // UTF-8 Endcoding
        PrintStream ps = new PrintStream(System.out,true, "UTF-8");

        // Comparator for WeightedMap's values(String)
        class StringBasedComparator implements Comparator<WeightedMap> {
            @Override
            public int compare(WeightedMap o1, WeightedMap o2) {
                if (o1.getValue().compareTo(o2.getValue()) > 0) {
                    return 1;
                } else if (o1.getValue().compareTo(o2.getValue()) < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }

        // Comparator for WeightedMap's weights(long)
        class WeightBasedComparator implements Comparator<WeightedMap> {
            @Override
            public int compare(WeightedMap o1, WeightedMap o2) {
                if (o1.getWeight() > o2.getWeight()){
                    return -1;
                }
                else if (o1.getWeight() < o2.getWeight()){
                    return 1;
                }
                else {
                    return 0;
                }
            }
        }

        // Finds array elements starting with Key
        class SearchBasedComparator implements Comparator<WeightedMap> {
            @Override
            public int compare(WeightedMap o1, WeightedMap o2) {
                if (o1.getValue().startsWith(o2.getValue())){
                    return 0;
                }
                else {
                    int i = 0;
                    int temp = 0;
                    String first = o1.getValue();
                    String second = o2.getValue();
                    // check two string char by char
                    while (i != first.length() && i != second.length()){
                        if (first.charAt(i) > second.charAt(i)){
                            temp = first.charAt(i) - second.charAt(i);
                            break;
                        }
                        else if (first.charAt(i) < second.charAt(i)){
                            temp = first.charAt(i) - second.charAt(i);
                            break;
                        }
                        else {
                            i++;
                        }
                    }
                    // it can be "abb" and "abbbb" so it is true but "abb" doesnt start with "abbbb"
                    // so check it
                    if (temp == 0){
                        if (o1.getValue().startsWith(o2.getValue())){
                            temp = 0;
                        }
                        else {
                            temp = -1;
                        }
                    }
                    return temp;
                }
            }
        }

        boolean FILE_IS_OK = false;  // User wants new file to search?
        String filePath = "";        // file Path


        while (true) {

            System.out.println("-------------------------------------------");
            System.out.println("|         Quick Search Algorithm          |");
            System.out.println("-------------------------------------------");

            System.out.println("Press E for Exit or Press any key to continue!");
            Scanner sc = new Scanner(System.in);
            String temp = sc.nextLine().trim();
            if (temp.equalsIgnoreCase("E")) {
                System.exit(0);
            }

            // User enters a file path
            if (!FILE_IS_OK) {
                System.out.println("Please enter file name:");
                Scanner scanner = new Scanner(System.in);
                File file = new File(scanner.nextLine().trim());

                // file path isnt exists
                while (!file.exists() || !file.isFile()) {
                    System.out.println("File not Found! Please enter a valid file name:");
                    Scanner scanner2 = new Scanner(System.in);
                    String fName = scanner2.nextLine().trim();
                    file = new File(fName);

                }

                filePath = file.getPath();
                FILE_IS_OK = true;
            }


            // User enters a K number
            boolean K_IS_OK = true;
            int k = 0;
            System.out.println("Please enter k number:");
            Scanner scanner = new Scanner(System.in);
            try {
                k = scanner.nextInt();
            } catch (Exception e) {
                K_IS_OK = false;
            }
            if (k < 0) {
                K_IS_OK = false;
            }
            // k is not valid
            while (!K_IS_OK) {
                System.out.println("ERROR! Please enter k number:");
                Scanner scanner3 = new Scanner(System.in);
                try {
                    k = scanner3.nextInt();
                    K_IS_OK = true;
                } catch (Exception e) {
                    K_IS_OK = false;
                    System.out.println("Wrong format for k! k must be an integer!");
                }

                if (K_IS_OK) {
                    if (k < 0) {
                        K_IS_OK = false;
                        //throw new IllegalArgumentException("k number cannot be less than 0");
                    } else {
                        K_IS_OK = true;
                    }
                }
            }

            // User enters a search Key
            System.out.println("Please Enter a search key:");
            Scanner scan = new Scanner(System.in);
            String key = scan.nextLine().trim();

            // key is not valid
            while (key.equals("") || key == null) {

                System.out.println("Invalid Key! Please Enter a search key:");
                Scanner scan2 = new Scanner(System.in);
                key = scan.nextLine().trim();

            }


            // Create an object for read file
            File file = new File(filePath);
            ReadFile readFile = new ReadFile(file.getPath());


            System.out.println("------------------------------------------------------------------");
            System.out.print("% ");
            System.out.print(file.getName() + " ");
            System.out.println(k);
            System.out.println(key);

            // reads given file and returns a arraylist
            ArrayList<WeightedMap> list = readFile.readFromFile();
            YGTStopwatch timer = new YGTStopwatch();
            // sort the list using QuickSort algorithm
            QuickSort<WeightedMap> quickSort = new QuickSort<>(list, new StringBasedComparator());


            // It does a binary search and
            // finds an element that matches the criterion provided by the "comparator"
            BinarySearch<WeightedMap> binarySearch = new BinarySearch<>(list, new SearchBasedComparator(), new WeightedMap(1, key));
            int index = binarySearch.getIndex();
            // index is valid
            if (index >= 0) {
                ArrayList<WeightedMap> foundedIndex = binarySearch.getFoundedKeys();
                new QuickSort<>(foundedIndex, new WeightBasedComparator());
                try {
                    for (int i = 0; i < k; i++) {
                        ps.printf("%d %s\n", foundedIndex.get(i).getWeight(), foundedIndex.get(i).getValue());
                    }
                } catch (Exception ignored) {

                }
                // key not found index is negative
            } else {
                System.out.println("KEY NOT FOUND!");
            }


            // prints execution time of sorting and searching
            System.out.println("\n ------------------------------");
            System.out.printf("| Search Execution Time = %.2f |\n", timer.elapsedTime());
            System.out.println(" ------------------------------");
            System.out.println("------------------------------------------------------------------");


            // change file or continue
            System.out.println("\n\nPress N to enter new file name or press any key for continue same file:");
            Scanner scanner1 = new Scanner(System.in);
            String newFile = scanner1.nextLine().trim();
            if (newFile.equalsIgnoreCase("N")) {
                FILE_IS_OK = false;
            }

        }
    }
}
