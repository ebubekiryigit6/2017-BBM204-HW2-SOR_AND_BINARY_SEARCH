import java.io.*;
import java.util.ArrayList;

/**
 * Created by Ebubekir.
 */
public class ReadFile {

    private String fileName;
    private long count = 0;

    public ReadFile (String fileName){
        this.fileName = fileName;
    }


    /**
     * Reads file and creates objects of WeightedMaps.
     *
     * @return
     * @throws IOException
     */
    public ArrayList<WeightedMap> readFromFile() throws IOException {

        ArrayList<WeightedMap> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
            String line;
            // read a line
            while ((line = br.readLine()) != null) {
                // update line count
                count++;
                line = line.trim();  // ignore spaces of line
                if (!line.equals("")) {  // ignore spaces of file
                    if (count != 1) {
                        String[] splitted = line.split("\t");
                        WeightedMap weightedMap = new WeightedMap(Long.parseLong(splitted[0]), splitted[1]);
                        list.add(weightedMap);
                    }
                }
            }

        return list;

    }

    public long getCount() {
        return count;
    }

    public String getFileName() {
        return fileName;
    }
}
