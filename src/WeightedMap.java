/**
 * Created by Ebubekir.
 */
public class WeightedMap {

    private long weight;
    private String value;


    public WeightedMap(long weight, String value) {
        this.weight = weight;
        this.value = value;
    }

    public long getWeight() {
        return weight;
    }

    public String getValue() {
        return value;
    }


}
