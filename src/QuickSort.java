import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Ebubekir.
 */
public class QuickSort<Vector> {

    private Comparator<Vector> comparator;  // Class sorts given list using this comparator
    private ArrayList<Vector> arrayList;    // Given arraylist to sort


    public QuickSort (ArrayList<Vector> list, Comparator<Vector> vectorComparator){
        if (list == null || vectorComparator == null){
            throw new IllegalArgumentException("Parameter Objects cannot be null");
        }
        this.arrayList = list;
        this.comparator = vectorComparator;
        Collections.shuffle(arrayList);             // shuffle for efficiency of QuickSort
        quickSort(0,arrayList.size()-1);   // do QuickSort
    }

    public ArrayList<Vector> getSortedList(){
        return this.arrayList;
    }

    /**
     * Quick Sort algorithm
     * The algorithm consists of two parts.
     * The first is the partition part, the second is the sort part.
     *
     * In the algorithm, a pivot element is selected and
     * the low and high values are updated by comparing with the given low, high values.
     * So the small ones on the left of the pivots are passed to the right on the right.
     * When this operation is repeated recursively, the array is sorted.
     *
     * @param low  array's low index element
     * @param high array's high index element
     */
    private void quickSort(int low, int high){
        int i = low;
        int j = high;

        // pivot
        Vector pivotElement = arrayList.get(low + (high - low)/2);

        do {

            while (comparator.compare(arrayList.get(i), pivotElement) < 0){
                i++;
            }

            while (comparator.compare(arrayList.get(j), pivotElement) > 0){
                j--;
            }

            // swap the elements
            if (i <= j){
                Collections.swap(arrayList,i,j);
                i++;
                j--;
            }
        } while (i <= j);

        if (low < j){
            quickSort(low,j);
        }
        if (i < high){
            quickSort(i,high);
        }

    }

}
